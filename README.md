# Notebooks - CNN - Détection de tumeurs cérébrales

Ces notebook sont en lien avec le dépôt Brain tumor détection API. Ce sont les notebooks qui ont permis d’entraîner les modèles de prédictions suivants :
 
* VGG19;
* Resnet50;
* Inception-ResnetV2.
